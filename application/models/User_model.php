<?php
defined('BASEPATH') OR exit ('No direct script access allowed');

class User_model extends CI_Model{
	
	public function check_user()

	{
		$username = $this->input->post('username');
		$password = $this->input->post('password');

		$query = $this->db->where('username' , $username)
						  ->where('password', $password)
						  ->get('user');

		if($query->num_rows() > 0)
		{

			$data_login = $query->row();
			$data = array(
				'id_user' 	=> $data_login->id_user,
				'username'	=> $data_login->username,
				'level' => $data_login->level,
				'logged_in'	=> TRUE
			);

				$this->session->set_userdata($data);

				return TRUE;
		}else {
			return FALSE;
		}

	}
	public function delete_trans($id)
	{
		$this->db->where('id_transaksi',$id);
		$this->db->delete('transaksi');
		return TRUE;
		
	}
	public function get_user()
	{
		return  $this->db->get('user')
					->result();
	}
	public function get_transaksi()
	{
		return  $this->db->get('transaksi')
					->result();
	}	
		
		public function get_sushi()
	{
		$cat = "Sushi";
		return  $this->db->where('kategori_makanan', $cat)
					->get('makanan')
					->result();
	}	
		public function detail($id)
	{
		return $this->db
		->where('id_makanan',$id)
		->get('makanan')
		->row();
	}
	public function get_ramen()
	{
		$cat = "Ramen";
		return  $this->db->where('kategori_makanan', $cat)
					->get('makanan')
					->result();
	}	
	public function get_salads()
	{
		$cat = "Salads";
		return  $this->db->where('kategori_makanan', $cat)
					->get('makanan')
					->result();
	}
	public function get_about()
	{
		return $this->db->get('about')
						->row();
	}
	public function get_contact()
	{
		return $this->db->get('contact')
						->row();
	}
	public function get_makanan()
	{
		return  $this->db->get('makanan')
					->result();
	}	
	public function get_keranjang()
	{
		return  $this->db->get('keranjang')
					->result();
	}	
	public function tambah_user()
	{
			$cat="member";
		$data = array(
			'nama_user' => $this->input->post('nama_user'),
			'username' => $this->input->post('username'),
			'password' => $this->input->post('password'),
			'level' => $cat
		);
		return $this->db->insert('user',$data);
	}
	public function tambah_saran()
	{
		$data = array(
			'tentang' => $this->input->post('tentang'),
			'saran' => $this->input->post('saran')
		);
		return $this->db->insert('saran',$data);
	}
	public function addchart($id_makanan,$nama_makanan,$harga_makanan){
	
			$data = array(
			'id_user' => $this->session->userdata('id_user'),
			'id_makanan' => $id_makanan,
			'nama_makanan' => $nama_makanan,
			'harga_makanan' =>  $harga_makanan
		);
		return $this->db->insert('keranjang',$data);
	}
	public function delete_chart($id_keranjang)
	{
		
		$this->db->where('id_keranjang',$id_keranjang)
						->delete('keranjang');
						return TRUE;
	}

	public function data_chart($id_user){

		return  $this->db->where('id_user', $id_user)
					->get('keranjang')
					->result();
	}
	public function harga_makanan($id_makanan){
			return  $this->db->where('id_makanan', $id_makanan)
					->get('makanan')
					->result();
	}
	public function addtransaction($id_keranjang){
			$data = array(
			'id_user' => $this->session->userdata('id_user'),
			'id_makanan' => $id_makanan,
			'nama_makanan' => $nama_makanan,
			'harga_makanan' =>  $harga_makanan
		);
		return $this->db->insert('keranjang',$data);
	}
	public function getDetailMakanan($id)
 	{
 		return $this->db->select('*')
 						->where('id_makanan',$id)
 						->get('makanan')
 						->row();
 	}
	public function update_makanan($id)
	{
		$data = array (
				
				'nama_makanan' => $this->input->post('nama_makanan'),
				'harga_makanan' => $this->input->post('harga_makanan'),
				'deskripsi_makanan' => $this->input->post('deskripsi_makanan'),
				'kategori_makanan' => $this->input->post('kategori_makanan')
				
			);

		$this->db->where('id_makanan', $id)
					->update('makanan', $data);

		if ($this->db->affected_rows() >0) {
			return true;
		}else {
			return false;
		}

	}
	public function getDetailUser($id)
 	{
 		return $this->db->select('*')
 						->where('id_user',$id)
 						->get('user')
 						->row();
 	}
	public function update_user($id)
	{
		$data = array (
				'id_user' => $this->input->post('id_user'),
				'nama_user' => $this->input->post('nama_user'),
				'username' => $this->input->post('username'),
				'password' => $this->input->post('password'),
				'level' => $this->input->post('level')
			);

		$this->db->where('id_user', $id)
					->update('user', $data);

		if ($this->db->affected_rows() >0) {
			return true;
		}else {
			return false;
		}

	}
	public function save_makanan($foto)
	{
		$data = array (
				
				'nama_makanan' => $this->input->post('nama_makanan'),
				'harga_makanan' => $this->input->post('harga_makanan'),
				'deskripsi_makanan' => $this->input->post('deskripsi_makanan'),
				'kategori_makanan' => $this->input->post('kategori_makanan'),
				'pic_makanan' => $foto['file_name']
			);
		$this->db->insert('makanan',$data);

		if ($this->db->affected_rows() >0) {
			return true;
		}else {
			return false;
		}

	}
	public function save_user()
	{
		$data = array (
				
				'nama_user' => $this->input->post('nama_user'),
				'username' => $this->input->post('username'),
				'password' => $this->input->post('password'),
				'level' => $this->input->post('level')
				
			);
		$this->db->insert('user',$data);

		if ($this->db->affected_rows() >0) {
			return true;
		}else {
			return false;
		}

	}
	public function getDataMakanan($id)
 	{
 		return $this->db->select('*')
 						->where('id_makanan', $id)
									->get('makanan')
									->row();
 	}
 	public function simpanTrans()
	{
		$object = array(
				'id_user'=>$this->session->userdata('id_user'),
				'nama_pembeli'=>$this->input->post('nama_pembeli'),
				'total'=>$this->cart->total(),
				'tanggal_beli'=>date('Y-m-d')
			);
		$this->db->insert('transaksi', $object);
		$tm_nota=$this->db->order_by('id_transaksi','desc')
						->where('nama_pembeli',$this->input->post('nama_pembeli'))
						->limit(1)
						->get('transaksi')
						->row();
		for ($i=0 ;$i<count($this->input->post('id_makanan'));$i++){
			$hasil[]=array(
				'id_transaksi'=>$tm_nota->id_transaksi,
				'jumlah'=>$this->input->post('qty')[$i],
				'id_makanan'=>$this->input->post('id_makanan')[$i]);
			/*$stok = array(
					'stok'=>$this->input->post('stok')[$i]-$this->input->post('qty')[$i],
				);
			$this->db->where('id_buku',$this->input->post('id_buku')[$i])
				->update('buku', $stok);*/
		}
		$proses=$this->db->insert_batch('nota',$hasil);
		if ($proses) {
			return $tm_nota->id_transaksi;
		} else {
			return 0;
		}
		
	}
	public function lastTrans()
	{
		return $this->db->order_by('id_transaksi','desc')
						->where('nama_pembeli',$this->input->post('nama_pembeli'))
						->limit(1)
						->get('transaksi')
						->row();
	}
	public function getDataNota($id)
	{
		return $this->db->join('makanan','makanan.id_makanan=nota.id_makanan')
						->where('id_transaksi',$id)
						->get('nota');
	}	
	public function getDataTransaksi($id)
	{
		return $this->db->join('user','user.id_user=transaksi.id_user')
						->where('id_transaksi',$id)
						->get('transaksi')->row();
	}
}
?>