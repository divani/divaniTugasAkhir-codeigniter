<!DOCTYPE html>
<html>
<title>Ja Panfud</title>
<meta charset="UTF-8">
<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="icon" type="image/png" href="<?php echo base_url() ?>/assets/img/logo.png">
<link rel="stylesheet" href="<?php echo base_url() ?>/assets/css/w3.css">
<style>
body, html {height: 100%}
body,h1,h2,h3,h4,h5,h6 {font-family: "Amatic SC", sans-serif}
.menu {display: none}
.bgimg {
    background-repeat: no-repeat;
    background-size: cover;
    background-image: url("<?php echo base_url() ?>/assets/img/sushii.jpg");
    min-height: 100%;
}
.btn:hover{
    opacity: 0.8;
}
.a{ 
  position:absolute;
  background:rgba(30,30,30,0.7);
  display:none;
  margin-top:40px;
  margin-left:90%;
  border-radius:0 0 30px 30px;
}
.b:hover .a{
  display:block;
}
ul{
  list-style: none;
  width: 130px;
}
ul li{
  margin-left: -45%;
  padding:0.5%;
  padding-left:30%;
  padding-right:30%;
}

ul li:hover{
  background:rgba(100,100,100,0.7);
}
</style>
<body>

<!
  
<!-- Header with image -->


<!-- Menu Container -->
<div class="w3-container w3-black w3-padding-64 w3-xxlarge" id="menu">
  <div class="w3-content">
    <?php if (!empty ($notif)) {
    echo '<div class="alert alert-danger">'.$notif.'</div>
    ';
}?>
  
    
    <h1 class="w3-center w3-jumbo" style="margin-bottom:64px">THE MENU<a href="<?php echo base_url();?>index.php/user/tambah_makanan/" class="w3-right w3-tag w3-blue w3-round" style="text-decoration: none;"> <i class="glyphicon glyphicon-plus"></i>+</a></h1>
 <div class="w3-row w3-center w3-border w3-border-dark-grey">
      <a href="javascript:void(0)" onclick="openMenu(event, 'Sushi');" id="myLink">
        <div class="w3-col s4 tablink w3-padding-large w3-hover-red">Sushi</div>
      </a>
      <a href="javascript:void(0)" onclick="openMenu(event, 'Salads');">
        <div class="w3-col s4 tablink w3-padding-large w3-hover-red">Salads</div>
      </a>
      <a href="javascript:void(0)" onclick="openMenu(event, 'Ramen');">
        <div class="w3-col s4 tablink w3-padding-large w3-hover-red">Ramen</div>
      </a>
    </div>


    <div id="Sushi" class="w3-container menu w3-padding-32 w3-white">
      <?php
        foreach($sushi as $s){ ?>
              
            <h1><b>  <?php echo $s->nama_makanan  ?> </b> <span class="w3-right w3-tag w3-dark-grey w3-round"><?php echo $s->harga_makanan ?> K</span></h1>
            <span class="w3-right w3-tag w3-pink w3-round"> <a  style="text-decoration: none" class="btn btn-success Edit" style="margin: 10px;"  href="edit_makanan/<?php echo $s->id_makanan; ?>" class="btn btn-info">Edit</a>
              </span>
              
          
            <p class="w3-text-blue" class="addc" style="font-family: tw cen mt;font-size: 20px;width: 80%"><?php echo $s->deskripsi_makanan ?></p>
              <form class="col-md-4" method="POST" action="<?php echo base_url();?>index.php/admin/delete_makanan/<?php echo $s->id_makanan ?>">
          <input   type="submit" style="text-decoration: none" class="btn btn-success Edit" style="margin: 10px;"  class="delete btn btn-danger"   style="margin: 10px;" aria-hidden="true" value="Delete"> </form>
             <hr>
             
   <?php }; ?>
    </div>

    <div id="Salads" class="w3-container menu w3-padding-32 w3-white">
       <?php
        foreach($salads as $sl){ ?>
              
            <h1><b>  <?php echo $sl->nama_makanan  ?> </b> <span class="w3-right w3-tag w3-dark-grey w3-round"><?php echo $sl->harga_makanan ?> K</span></h1>
            <span class="w3-right w3-tag w3-pink w3-round"> <a  style="text-decoration: none" class="btn btn-success Edit" style="margin: 10px;"  href="edit_makanan/<?php echo $s->id_makanan; ?>" class="btn btn-info">Edit</a>

              </span>
              
            <p class="w3-text-blue" class="addc" style="font-family: tw cen mt;font-size: 20px;width: 80%"><?php echo $sl->deskripsi_makanan ?></p>
              <form class="col-md-4" method="POST" action="<?php echo base_url();?>index.php/admin/delete_makanan/<?php echo $s->id_makanan ?>">
          <input   type="submit" style="text-decoration: none" class="btn btn-success Edit" style="margin: 10px;"  class="delete btn btn-danger"   style="margin: 10px;" aria-hidden="true" value="Delete"> </form>
             <hr>
             
   <?php }; ?>
     
    </div>


    <div id="Ramen" class="w3-container menu w3-padding-32 w3-white">
       <?php
        foreach($ramen as $r){ ?>
              
            <h1><b>  <?php echo $r->nama_makanan  ?> </b> <span class="w3-right w3-tag w3-dark-grey w3-round"><?php echo $r->harga_makanan ?> K</span></h1>
            <span class="w3-right w3-tag w3-pink w3-round"> <a  style="text-decoration: none" class="btn btn-success Edit" style="margin: 10px;"  href="edit_makanan/<?php echo $s->id_makanan; ?>" class="btn btn-info">Edit</a>

              </span>
              
            <p class="w3-text-blue" class="addc" style="font-family: tw cen mt;font-size: 20px;width: 80%"><?php echo $r->deskripsi_makanan ?></p>
              <form class="col-md-4" method="POST" action="<?php echo base_url();?>index.php/admin/delete_makanan/<?php echo $s->id_makanan ?>">
          <input   type="submit" style="text-decoration: none" class="btn btn-success Edit" style="margin: 10px;"  class="delete btn btn-danger"   style="margin: 10px;" aria-hidden="true" value="Delete"> </form>
             <hr>
             
   <?php }; ?>
    </div><br>

  </div>
</div>

<!-- About Container -->
<div class="w3-container w3-padding-64 w3-red w3-grayscale w3-xlarge" id="user">
  <div class="w3-content">
    <h1 class="w3-center w3-jumbo" style="margin-bottom:64px">User<a href="<?php echo base_url();?>index.php/user/tambah_user/" class="w3-right w3-tag w3-blue w3-round" style="text-decoration: none;"> <i class="glyphicon glyphicon-plus"></i>+</a></h1>
    <table width="100%" class="table table-striped table-bordered table-hover" border="1" id="dataTables-example">
                                <thead>
                                    <tr>
                                        <th>ID User</th>
                                        <th>Nama User</th>
                                        <th>Username</th>
                                        <th>Password</th>
                                        <th>level</th>
                                        <th>Aksi</th>
                                    </tr>
                                </thead>
                                <tbody>

                                <?php
                                foreach ($user as $userr) { ?>
                                <tr>
                                    <td>
                                        <center><?php echo $userr->id_user ?></center>
                                    </td>
                                    <td>
                                          <center><?php echo $userr->nama_user ?></center>
                                    </td>
                                    <td>
                                          <center><?php echo $userr->username ?></center>
                                    </td>
                                    <td>
                                          <center><?php echo $userr->password ?></center>
                                    </td>
                                     <td>
                                          <center><?php echo $userr->level ?></center>
                                    </td>
                                    <td>

                
            <center>  <span class="w3-right w3-tag w3-white w3-round" style="margin-top: 5%;margin-right: 10%"> <a  style="text-decoration: none;font-size: 30px;" class="btn btn-success Edit" href="edit_user/<?php echo $userr->id_user; ?>" class="btn btn-info">Edit</a></span>

           <form class="col-md-4" method="POST" action="<?php echo base_url();?>index.php/user/delete_user/<?php echo $userr->id_user ?>">
          <input type="submit" class="delete btn btn-danger"  style="margin: 10px;" aria-hidden="true" value="Delete"> </form>
          </center>
         
                                          
                                    </td>
                                </tr>
                                
                                    <?php }; ?>
                               
                                </tbody>
                            </table>
   
    
  </div>
</div>

<div class="w3-top w3-hide-small">
  <div class="w3-bar w3-xlarge w3-black w3-opacity w3-hover-opacity-off" id="myNavbar">
    <a href="#menu" class="w3-bar-item w3-button">FOOD</a>
    <a href="#user" class="w3-bar-item w3-button">USER</a>
    <div class="b"><a  style="text-decoration:none;float: right;margin-right: 2%;margin-top: 0.8%;font-size: 20px"><?php echo $username ?> | <?php echo $level ?>
    <div class="a">
        <ul>
          
          <li>
          <a href="logout" style="text-decoration:none;width:5%;">Logout</a>
          </li>
      </ul>
      </div></a></div>
  </div>
  </div>
</div>
<!-- Footer 
<footer class="w3-center w3-black w3-padding-48 w3-xxlarge">
  <p>Powered by <a href="https://www.w3schools.com/w3css/default.asp" title="W3.CSS" target="_blank" class="w3-hover-text-green">w3.css</a></p>
</footer>
-->

<!-- Add Google Maps -->
<script>
// Tabbed Menu
function openMenu(evt, menuName) {
  var i, x, tablinks;
  x = document.getElementsByClassName("menu");
  for (i = 0; i < x.length; i++) {
     x[i].style.display = "none";
  }
  tablinks = document.getElementsByClassName("tablink");
  for (i = 0; i < x.length; i++) {
     tablinks[i].className = tablinks[i].className.replace(" w3-red", "");
  }
  document.getElementById(menuName).style.display = "block";
  evt.currentTarget.firstElementChild.className += " w3-red";
}
document.getElementById("myLink").click();
</script>
<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyBu-916DdpKAjTmJNIgngS6HL_kDIKU0aU&callback=myMap"></script>
<!--
To use this code on your website, get a free API key from Google.
Read more at: https://www.w3schools.com/graphics/google_maps_basic.asp
-->
 
<script src="<?php echo base_url(); ?>bower_components/jquery/dist/jquery.min.js"></script>
<script src="<?php echo base_url(); ?>bower_components/jquery-ui/jquery-ui.min.js"></script>
<script src="<?php echo base_url(); ?>bower_components/bootstrap/dist/js/bootstrap.min.js"></script>


    <script type="text/javascript">
        $('.update_user').on('click',function()
        {
            var id =$(this).attr('data-id');
            $('#id_user').val(id);

             var name =$(this).attr('data-name');
            $('#nama_user').val(name);

             var username =$(this).attr('username');
            $('#username').val(username);

             var password =$(this).attr('password');
            $('#password').val(password);

             var level =$(this).attr('level');
            $('#level').val(level);

        })
    </script>


</body>
</html>

