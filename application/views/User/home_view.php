<!DOCTYPE html>
<html>
<title>JA PANFUD</title>
<meta charset="UTF-8">
<meta name="viewport" content="width=device-width, initial-scale=1">
		<link rel="icon" type="image/png" href="<?php echo base_url() ?>/assets/img/logo.png">
<link rel="stylesheet" href="<?php echo base_url() ?>/assets/css/w3.css">
<link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Amatic+SC">
<style>
body, html {height: 100%;
    background:url('<?php echo base_url() ?>/assets/img/back2.jpg');
}

body,h1,h2,h3,h4,h5,h6 {font-family: "Amatic SC", sans-serif}
.menu {display: none}
.bgimg {
    background-repeat: no-repeat;
    background-size: cover;
    background-image: url("<?php echo base_url() ?>/assets/img/sushii.jpg");
    min-height: 100%;
}
.bgimg:hover{
  opacity: 0.90;
  animation: trans 2s;
}
@keyframes trans{
  0%{
   opacity: 1;
  }
  100%{
    opacity: 0.9;
  }
}
.btn:hover{
    opacity: 0.8;
}
.a{	
	position:absolute;
	background:rgba(30,30,30,0.7);
	display:none;
	margin-top:40px;
	margin-left:90%;
	border-radius:0 0 30px 30px;
}
.b:hover .a{
	display:block;
}
ul{
  list-style: none;
  width: 130px;
}
ul li{
  margin-left: -45%;
  padding:0.5%;
  padding-left:30%;
  padding-right:30%;
}

ul li:hover{
	background:rgba(100,100,100,0.7);
}
.kotak{
  padding: 5%;
  width: 30%;
  margin-left: 3%;
  float: left;
  background: rgba(30,30,30,0.8);
  margin-top: 2% ;
  border: 2px solid black;
  border-radius: 10px;
}
.kotak:hover{
    border: 2px solid red;
    background: none;
    animation: 1s wow;
}
@keyframes wow{
  0%{
     background: rgba(30,30,30,0.8);
     border: 2px solid black;
  }
  100%{
     border: 2px solid red;
    background: none;
  }
}
</style>
<body>

<!-- Navbar (sit on top) -->
<div class="w3-top w3-hide-small">
  <div class="w3-bar w3-xlarge w3-black w3-opacity w3-hover-opacity-off" id="myNavbar">
    <a href="#" class="w3-bar-item w3-button">HOME</a>
    <a href="#menu" class="w3-bar-item w3-button">MENU</a>
    <a href="#about" class="w3-bar-item w3-button">ABOUT</a>
    <a href="#contact" class="w3-bar-item w3-button">CONTACT</a>

    <div class="b"><a  style="text-decoration:none;float: right;margin-right: 2%;margin-top: 0.8%;font-size: 20px"><?php echo $username ?> | <?php echo $level ?>
		<div class="a">
        <ul>
          <li>
          <a href="logout" style="text-decoration:none;width:5%;">Logout</a>
          </li>
      </ul>
			</div>
		</a>
	</div>
  </div>
</div>
  
<!-- Header with image -->
<header class="bgimg w3-display-container w3-grayscale-min" id="home">
  <div class="w3-display-bottomleft w3-padding">
    <span class="w3-tag w3-xlarge">Open from 10am to 12pm</span>
  </div>
  <div class="w3-display-middle w3-center">
    <span class="w3-text-white  w3-hide-small" style="font-size:100px">best deal<br> OF SUSHI</span>
    <span class="w3-text-white w3-hide-large w3-hide-medium" style="font-size:60px"><b>thin<br>CRUST PIZZA</b></span>
    <p><a href="#menu" class="w3-button w3-xxlarge w3-black">Let me see the menu</a></p>
  </div>
</header>

<!-- Menu Container -->
<div class="w3-container w3-padding-64 w3-xxlarge" id="menu" style="background: rgba(0,0,0,0.9);color: white">
  
  <div class="w3-content">
  
    <h1 class="w3-center w3-jumbo" style="margin-bottom:64px">THE MENU</h1>
    <div class="w3-row w3-center w3-border w3-border-dark-grey">
      <a href="javascript:void(0)" onclick="openMenu(event, 'Sushi');" id="myLink">
        <div class="w3-col s4 tablink w3-padding-large w3-hover-red">Sushi</div>
      </a>
      <a href="javascript:void(0)" onclick="openMenu(event, 'Salads');">
        <div class="w3-col s4 tablink w3-padding-large w3-hover-red">Salads</div>
      </a>
      <a href="javascript:void(0)" onclick="openMenu(event, 'Ramen');">
        <div class="w3-col s4 tablink w3-padding-large w3-hover-red">Ramen</div>
      </a>
    </div>

    <div id="Sushi" class="w3-container menu w3-padding-32 ">
      <?php
        foreach($sushi as $s){ ?>
        <div class="kotak">
              <img src="<?php echo base_url() ?>/upload/<?php echo $s->pic_makanan ; ?>" width="200px" >
            <h1><b>  <?php echo $s->nama_makanan  ?> </b></h1>
            
          

            <p class="w3-text-blue" class="addc" style="font-family: tw cen mt;font-size: 14px;width: 60%;float: left;margin-top: 0;height: 60px"><?php echo $s->deskripsi_makanan ?></p>
             <span class="w3-right w3-tag w3-dark-grey w3-round"><?php echo $s->harga_makanan ?> K</span>
          
          </div>
            
             
   <?php }; ?>
    </div>

    <div id="Salads" class="w3-container menu w3-padding-32 ">
       <?php
        foreach($salads as $sl){ ?>
            <div class="kotak">
              <img src="<?php echo base_url() ?>/upload/<?php echo $sl->pic_makanan ; ?>"  width="200px" >
            <h1 style="font-size: 35px"><b>  <?php echo $sl->nama_makanan  ?> </b></h1>
            
          

            <p class="w3-text-blue" class="addc" style="font-family: tw cen mt;font-size: 14px;width: 60%;float: left;margin-top: 0;height: 60px"><?php echo $sl->deskripsi_makanan ?></p>
             <span class="w3-right w3-tag w3-dark-grey w3-round"><?php echo $sl->harga_makanan ?> K</span>
           
          </div>
            
   <?php }; ?>
     
    </div>


    <div id="Ramen" class="w3-container menu w3-padding-32 ">
       <?php
        foreach($ramen as $r){ ?>
              
             <div class="kotak">
              <img src="<?php echo base_url() ?>/upload/<?php echo $r->pic_makanan ; ?>" width="200px" >
            <h1><b>  <?php echo $r->nama_makanan  ?> </b></h1>
            
          

            <p class="w3-text-blue" class="addc" style="font-family: tw cen mt;font-size: 14px;width: 60%;float: left;margin-top: 0;height: 60px"><?php echo $r->deskripsi_makanan ?></p>
             <span class="w3-right w3-tag w3-dark-grey w3-round"><?php echo $r->harga_makanan ?> K</span>
       
          </div>
            
             
   <?php }; ?>
    </div><br>

  </div>
</div>

<!-- About Container -->
<div class="w3-container w3-padding-64 w3-red w3-grayscale w3-xlarge" id="about">
  <div class="w3-content">
    <h1 class="w3-center w3-jumbo" style="margin-bottom:64px">About</h1>
    <p>
      <?php echo $about->about_desc; ?>
        
      </p>
    <p><strong>The Chef?</strong> Mr. Italiano himself<img src="<?php echo base_url() ?>/assets/img/chef.jpg" style="width:150px" class="w3-circle w3-right" alt="Chef"></p>
    <p>We are proud of our interiors.</p>
    <img src="<?php echo base_url() ?>/assets/img/onepage_restaurant.jpg" style="width:100%" class="w3-margin-top w3-margin-bottom" alt="Restaurant">
    <h1><b>Opening Hours</b></h1>
    
    <div class="w3-row">
      <div class="w3-col s6">
        <p>Mon & Tue CLOSED</p>
        <p>Wednesday 10.00 - 24.00</p>
        <p>Thursday 10:00 - 24:00</p>
      </div>
      <div class="w3-col s6">
        <p>Friday 10:00 - 12:00</p>
        <p>Saturday 10:00 - 23:00</p>
        <p>Sunday Closed</p>
      </div>
    </div>
    
  </div>
</div>

<!-- Contact (with google maps) -->

<div class="w3-container w3-padding-64 w3-blue-grey w3-grayscale-min w3-xlarge" id="contact">
  <div class="w3-content">
    <h1 class="w3-center w3-jumbo" style="margin-bottom:64px">Contact</h1>
    <p>Find us at some address at some place or call us at <?php echo $contact->contact_desc ?> ( <?php echo $contact->address ?>)</p>
    <p><span class="w3-tag">FYI!</span> We offer full-service catering for any event, large or small. We understand your needs and we will cater the food to satisfy the biggerst criteria of them all, both look and taste.</p>
    <p class="w3-xxlarge">From this website, you can give us <strong>SUGGESTION</strong> :</p>
     <form role="form" action="<?php echo base_url('index.php/user/tambah_saran'); ?>" method="post" >
      <p><input class="w3-input w3-padding-16 w3-border" type="text" placeholder="About" required name="tentang"></p>
      <p><input class="w3-input w3-padding-16 w3-border" type="text" placeholder="Suggestion..." required name="saran"></p> 
      <p><button class="w3-button w3-light-grey w3-block" type="submit">SEND</button></p>
    </form>
  </div>
</div>

<!-- Footer
<footer class="w3-center w3-black w3-padding-48 w3-xxlarge">
  <p>Powered by <a href="https://www.w3schools.com/w3css/default.asp" title="W3.CSS" target="_blank" class="w3-hover-text-green">w3.css</a></p>
</footer>
 -->
 
<!-- Add Google Maps -->
<script>
function myMap()
{
  myCenter=new google.maps.LatLng(41.878114, -87.629798);
  var mapOptions= {
    center:myCenter,
    zoom:12, scrollwheel: false, draggable: false,
    mapTypeId:google.maps.MapTypeId.ROADMAP
  };
  var map=new google.maps.Map(document.getElementById("googleMap"),mapOptions);

  var marker = new google.maps.Marker({
    position: myCenter,
  });
  marker.setMap(map);
}

// Tabbed Menu
function openMenu(evt, menuName) {
  var i, x, tablinks;
  x = document.getElementsByClassName("menu");
  for (i = 0; i < x.length; i++) {
     x[i].style.display = "none";
  }
  tablinks = document.getElementsByClassName("tablink");
  for (i = 0; i < x.length; i++) {
     tablinks[i].className = tablinks[i].className.replace(" w3-red", "");
  }
  document.getElementById(menuName).style.display = "block";
  evt.currentTarget.firstElementChild.className += " w3-red";
}
document.getElementById("myLink").click();
</script>
<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyBu-916DdpKAjTmJNIgngS6HL_kDIKU0aU&callback=myMap"></script>
<!--
To use this code on your website, get a free API key from Google.
Read more at: https://www.w3schools.com/graphics/google_maps_basic.asp
-->



</body>
</html>
