-- phpMyAdmin SQL Dump
-- version 4.7.4
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: May 28, 2018 at 01:53 PM
-- Server version: 10.1.30-MariaDB
-- PHP Version: 5.6.33

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `tugasakhir`
--

-- --------------------------------------------------------

--
-- Table structure for table `about`
--

CREATE TABLE `about` (
  `id_about` int(11) NOT NULL,
  `about_desc` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `about`
--

INSERT INTO `about` (`id_about`, `about_desc`) VALUES
(1, 'Sushi,Salads, & Ramen');

-- --------------------------------------------------------

--
-- Table structure for table `contact`
--

CREATE TABLE `contact` (
  `id_contact` int(11) NOT NULL,
  `address` varchar(300) NOT NULL,
  `contact_desc` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `contact`
--

INSERT INTO `contact` (`id_contact`, `address`, `contact_desc`) VALUES
(1, '081234567890', 'WA');

-- --------------------------------------------------------

--
-- Table structure for table `keranjang`
--

CREATE TABLE `keranjang` (
  `id_keranjang` int(11) NOT NULL,
  `id_user` int(11) NOT NULL,
  `id_makanan` int(11) NOT NULL,
  `nama_makanan` varchar(300) NOT NULL,
  `harga_makanan` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `keranjang`
--

INSERT INTO `keranjang` (`id_keranjang`, `id_user`, `id_makanan`, `nama_makanan`, `harga_makanan`) VALUES
(14, 9, 4, 'Salmon Roll', 30),
(15, 9, 5, 'Beef Dry Ramen', 35),
(16, 9, 1, 'California Roll', 35),
(17, 9, 3, 'Fish Cake Roll', 40),
(18, 9, 4, 'Salmon Roll', 30),
(19, 9, 12, 'Italian Chopped Salad', 40),
(20, 8, 5, 'Beef Dry Ramen', 35),
(21, 8, 3, 'Fish Cake Roll', 40),
(24, 8, 10, 'Niku Udon', 30),
(27, 8, 2, 'Dragon Ball Roll', 40),
(31, 8, 4, 'Salmon Roll', 30),
(32, 8, 1, 'California Roll', 35),
(33, 8, 1, 'California Roll', 35),
(34, 10, 5, 'Beef Dry Ramen', 35),
(35, 10, 3, 'Fish Cake Roll', 40),
(36, 10, 1, 'California Roll', 31),
(37, 10, 1, 'California Roll', 31),
(38, 10, 3, 'Fish Cake Roll', 40),
(39, 10, 1, 'California Roll', 31),
(40, 10, 1, 'California Roll', 31);

-- --------------------------------------------------------

--
-- Table structure for table `makanan`
--

CREATE TABLE `makanan` (
  `id_makanan` int(11) NOT NULL,
  `nama_makanan` varchar(200) NOT NULL,
  `harga_makanan` int(11) NOT NULL,
  `deskripsi_makanan` text NOT NULL,
  `kategori_makanan` varchar(100) NOT NULL,
  `pic_makanan` varchar(500) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `makanan`
--

INSERT INTO `makanan` (`id_makanan`, `nama_makanan`, `harga_makanan`, `deskripsi_makanan`, `kategori_makanan`, `pic_makanan`) VALUES
(1, 'California Roll', 31, 'Crab stick, telur, kyurinya, alpukat', 'Sushi', '2.jpg'),
(3, 'Fish Cake Roll', 40, 'Tomago, spicy mayonnaise, Tobiko', 'Sushi', '7.jpg'),
(5, 'Beef Dry Ramen', 35, 'Mie ramen, beef teriyaki, sayuran', 'Ramen', 'RAMEN.jpg'),
(6, 'Beef curry udon', 45, 'Beef sukiyaki, gyoza,kuah kari ', 'Ramen', 'RAMENN.jpg'),
(11, 'Tuna Salad', 30, 'Tuna, Mayo', 'Salads', 'SALAD.jpg'),
(15, 'Taco Salad', 45, 'Keripik tortilla, cheddar-jack dan krim asam', 'Salads', 'SALADS.jpg'),
(16, 'Salmon Roll', 31, 'Ikan salmon, sushi rice, nori', 'Sushi', '4.jpg');

-- --------------------------------------------------------

--
-- Table structure for table `nota`
--

CREATE TABLE `nota` (
  `id_nota` int(11) NOT NULL,
  `id_transaksi` int(11) NOT NULL,
  `id_makanan` int(11) NOT NULL,
  `jumlah` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `nota`
--

INSERT INTO `nota` (`id_nota`, `id_transaksi`, `id_makanan`, `jumlah`) VALUES
(7, 21, 3, 1),
(8, 22, 5, 2),
(9, 23, 3, 1),
(10, 24, 5, 1),
(11, 25, 1, 1),
(12, 25, 5, 1);

-- --------------------------------------------------------

--
-- Table structure for table `saran`
--

CREATE TABLE `saran` (
  `id_saran` int(11) NOT NULL,
  `tentang` varchar(300) NOT NULL,
  `saran` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `saran`
--

INSERT INTO `saran` (`id_saran`, `tentang`, `saran`) VALUES
(1, 'Pengiriman', 'Tidak datang lebih dari 30 menit'),
(2, 'Makanan', 'Makanannya berbau tidak enak'),
(3, 'harga terlalu mahal', 'disesuaikan kantong pelajar'),
(4, 'Pengantar', 'tidak sopan, sebaiknya dicari pengantar lain');

-- --------------------------------------------------------

--
-- Table structure for table `transaksi`
--

CREATE TABLE `transaksi` (
  `id_transaksi` int(11) NOT NULL,
  `id_user` int(11) NOT NULL,
  `nama_pembeli` varchar(500) NOT NULL,
  `total` int(11) NOT NULL,
  `tanggal_beli` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `transaksi`
--

INSERT INTO `transaksi` (`id_transaksi`, `id_user`, `nama_pembeli`, `total`, `tanggal_beli`) VALUES
(3, 11, '1', 31, '0000-00-00'),
(4, 10, 'aang', 71, '2018-05-28'),
(5, 10, 'aang', 71, '2018-05-28'),
(6, 10, 'aang', 71, '2018-05-28'),
(7, 10, 'aang', 71, '2018-05-28'),
(8, 10, 'aang', 71, '2018-05-28'),
(9, 10, 'aang', 71, '2018-05-28'),
(10, 10, 'aang', 71, '2018-05-28'),
(11, 10, 'aang', 71, '2018-05-28'),
(12, 10, 'aang', 71, '2018-05-28'),
(13, 10, 'aang', 71, '2018-05-28'),
(14, 10, 'aang', 0, '2018-05-28'),
(15, 10, 'aang', 0, '2018-05-28'),
(16, 10, 'aang', 0, '2018-05-28'),
(17, 10, 'aang', 0, '2018-05-28'),
(18, 10, 'aweng', 40, '2018-05-28'),
(19, 10, 'aweng', 40, '2018-05-28'),
(20, 10, 'aweng', 40, '2018-05-28'),
(21, 10, 'awer', 40, '2018-05-28'),
(22, 10, 'awertu', 70, '2018-05-28'),
(23, 10, 'erto', 40, '2018-05-28'),
(24, 10, 'artur', 35, '2018-05-28'),
(25, 11, 'indah', 66, '2018-05-28');

-- --------------------------------------------------------

--
-- Table structure for table `user`
--

CREATE TABLE `user` (
  `id_user` int(11) NOT NULL,
  `nama_user` varchar(300) NOT NULL,
  `username` varchar(300) NOT NULL,
  `password` varchar(300) NOT NULL,
  `level` varchar(300) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `user`
--

INSERT INTO `user` (`id_user`, `nama_user`, `username`, `password`, `level`) VALUES
(7, 'indah', 'indah', 'indah', 'admin'),
(10, 'yofika', 'yofika', 'fika', 'member'),
(11, 'diva', 'diva', 'diva', 'kasir'),
(12, 'jeje', 'jeje', 'jeje', 'admin'),
(13, 'enrico', 'enrico', 'enrico', 'member');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `about`
--
ALTER TABLE `about`
  ADD PRIMARY KEY (`id_about`);

--
-- Indexes for table `contact`
--
ALTER TABLE `contact`
  ADD PRIMARY KEY (`id_contact`);

--
-- Indexes for table `keranjang`
--
ALTER TABLE `keranjang`
  ADD PRIMARY KEY (`id_keranjang`),
  ADD KEY `id_user` (`id_user`),
  ADD KEY `id_makanan` (`id_makanan`);

--
-- Indexes for table `makanan`
--
ALTER TABLE `makanan`
  ADD PRIMARY KEY (`id_makanan`);

--
-- Indexes for table `nota`
--
ALTER TABLE `nota`
  ADD PRIMARY KEY (`id_nota`);

--
-- Indexes for table `saran`
--
ALTER TABLE `saran`
  ADD PRIMARY KEY (`id_saran`);

--
-- Indexes for table `transaksi`
--
ALTER TABLE `transaksi`
  ADD PRIMARY KEY (`id_transaksi`),
  ADD KEY `id_user` (`id_user`),
  ADD KEY `id_makanan` (`nama_pembeli`);

--
-- Indexes for table `user`
--
ALTER TABLE `user`
  ADD PRIMARY KEY (`id_user`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `about`
--
ALTER TABLE `about`
  MODIFY `id_about` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `contact`
--
ALTER TABLE `contact`
  MODIFY `id_contact` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `keranjang`
--
ALTER TABLE `keranjang`
  MODIFY `id_keranjang` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=41;

--
-- AUTO_INCREMENT for table `makanan`
--
ALTER TABLE `makanan`
  MODIFY `id_makanan` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=17;

--
-- AUTO_INCREMENT for table `nota`
--
ALTER TABLE `nota`
  MODIFY `id_nota` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;

--
-- AUTO_INCREMENT for table `saran`
--
ALTER TABLE `saran`
  MODIFY `id_saran` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `transaksi`
--
ALTER TABLE `transaksi`
  MODIFY `id_transaksi` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=26;

--
-- AUTO_INCREMENT for table `user`
--
ALTER TABLE `user`
  MODIFY `id_user` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=14;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
