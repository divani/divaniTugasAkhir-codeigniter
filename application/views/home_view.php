<!DOCTYPE html>
<html>
<title>Tugas Akhir</title>
<meta charset="UTF-8">
<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="icon" type="image/png" href="<?php echo base_url() ?>/assets/img/logo.png">
<link rel="stylesheet" href="<?php echo base_url() ?>/assets/css/w3.css">
<link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Amatic+SC">
<style>
body, html {height: 100%;  background:url('<?php echo base_url() ?>/assets/img/back2.jpg');}
body,h1,h2,h3,h4,h5,h6 {font-family: "Amatic SC", sans-serif}
.menu {display: none}
.bgimg {
    background-repeat: no-repeat;
    background-size: cover;
    background-image: url("<?php echo base_url() ?>/assets/img/sushii.jpg");
    min-height: 100%;
}
.btn:hover{
    opacity: 0.8;
}

.kotak{
  padding: 5%;
  width: 30%;
  margin-left: 3%;
  float: left;
  background: rgba(30,30,30,0.8);
  margin-top: 2% ;
  border: 2px solid black;
  border-radius: 10px;
}
.kotak:hover{
    border: 2px solid red;
    background: none;
    animation: 1s wow;
}
@keyframes wow{
  0%{
     background: rgba(30,30,30,0.8);
     border: 2px solid black;
  }
  100%{
     border: 2px solid red;
    background: none;
  }
}

 
.peta{
    background: #e6eced;
    width: 100%;
    height: 550px;
}
.peta:hover{
    background: rgba(0,0,0,0.8);
}
.peta:hover h1{
    color: white
}
.peta img{
    width: 75%;
    margin-top: 10%;

}
.image img{
   
    position: absolute; 
    width: 2%;
}
@keyframes kdip {
    0%{
        opacity: 0.5;
    }
    100%{
        opacity: 1;
    }
}
.judul-map{
    position: absolute;
    text-align: center;
    font-family: Amatic SC;
    font-size: 40px;
    padding-top: 5%;
    width: 100%;
    margin-top: -3%;
}
.nama-lokasi{
    border-radius: 30px;
    padding-left:  1%;
    padding-right:  1%;
    background: rgba(230,230,230,0.9);
    color:#ff383f;
    opacity: 0;
    position: absolute;
    font-family: century gothic;
    text-decoration: none;
    font-size: 20px;
}
.image:hover .nama-lokasi{
    opacity: 1;
}

</style>
<body>

<!-- Navbar (sit on top) -->
<div class="w3-top w3-hide-small">
  <div class="w3-bar w3-xlarge w3-black w3-opacity w3-hover-opacity-off" id="myNavbar">
    <a href="#" class="w3-bar-item w3-button">HOME</a>
    <a href="#menu" class="w3-bar-item w3-button">MENU</a>
    <a href="#about" class="w3-bar-item w3-button">ABOUT</a>
  </div>
</div>
  
<!-- Header with image -->
<header class="bgimg w3-display-container w3-grayscale-min" id="home">
  <div class="w3-display-bottomleft w3-padding">
    <span class="w3-tag w3-xlarge">Open from 10am to 12pm</span>
  </div>
  <div class="w3-display-middle w3-center">
    <span class="w3-text-white  w3-hide-small" style="font-size:100px">best deal<br> OF SUSHI</span>
    <span class="w3-text-white w3-hide-large w3-hide-medium" style="font-size:60px"><b>thin<br>CRUST PIZZA</b></span>
    <p><a href="index.php/user/login" class="w3-button w3-xxlarge w3-black">Let's Login</a></p>
  </div>
</header>
<div class="w3-container w3-padding-64 w3-xxlarge" id="menu" style="background: rgba(0,0,0,0.9);color: white">
  
  <div class="w3-content">
  
    <h1 class="w3-center w3-jumbo" style="margin-bottom:64px">THE MENU</h1>
    <div class="w3-row w3-center w3-border w3-border-dark-grey">
      <a href="javascript:void(0)" onclick="openMenu(event, 'Sushi');" id="myLink">
        <div class="w3-col s4 tablink w3-padding-large w3-hover-red">Sushi</div>
      </a>
      <a href="javascript:void(0)" onclick="openMenu(event, 'Salads');">
        <div class="w3-col s4 tablink w3-padding-large w3-hover-red">Salads</div>
      </a>
      <a href="javascript:void(0)" onclick="openMenu(event, 'Ramen');">
        <div class="w3-col s4 tablink w3-padding-large w3-hover-red">Ramen</div>
      </a>
    </div>

    <div id="Sushi" class="w3-container menu w3-padding-32 ">
      <?php
        foreach($sushi as $s){ ?>
        <div class="kotak">
              <img src="<?php echo base_url() ?>/upload/<?php echo $s->pic_makanan ; ?>" width="200px" >
            <h1><b>  <?php echo $s->nama_makanan  ?> </b></h1>
            
          

            <p class="w3-text-blue" class="addc" style="font-family: tw cen mt;font-size: 14px;width: 60%;float: left;margin-top: 0;height: 60px"><?php echo $s->deskripsi_makanan ?></p>
             <span class="w3-right w3-tag w3-dark-grey w3-round"><?php echo $s->harga_makanan ?> K</span>
           
          </div>
            
             
   <?php }; ?>
    </div>

    <div id="Salads" class="w3-container menu w3-padding-32 ">
       <?php
        foreach($salads as $sl){ ?>
            <div class="kotak">
              <img src="<?php echo base_url() ?>/upload/<?php echo $sl->pic_makanan ; ?>" width="200px" >
            <h1 style="font-size: 35px"><b>  <?php echo $sl->nama_makanan  ?> </b></h1>
            
          

            <p class="w3-text-blue" class="addc" style="font-family: tw cen mt;font-size: 14px;width: 60%;float: left;margin-top: 0;height: 60px"><?php echo $sl->deskripsi_makanan ?></p>
             <span class="w3-right w3-tag w3-dark-grey w3-round"><?php echo $sl->harga_makanan ?> K</span>
           
          </div>
            
   <?php }; ?>
     
    </div>


    <div id="Ramen" class="w3-container menu w3-padding-32 ">
       <?php
        foreach($ramen as $r){ ?>
              
             <div class="kotak">
              <img src="<?php echo base_url() ?>/upload/<?php echo $r->pic_makanan ; ?>" width="200px" >
            <h1><b>  <?php echo $r->nama_makanan  ?> </b></h1>
            
          

            <p class="w3-text-blue" class="addc" style="font-family: tw cen mt;font-size: 14px;width: 60%;float: left;margin-top: 0;height: 60px"><?php echo $r->deskripsi_makanan ?></p>
             <span class="w3-right w3-tag w3-dark-grey w3-round"><?php echo $r->harga_makanan ?> K</span>
            
          </div>
            
             
   <?php }; ?>
    </div><br>

  </div>
</div>

<!-- About Container -->
<div class="w3-container w3-padding-64 w3-red w3-grayscale w3-xlarge" id="about">
  <div class="w3-content">
    <h1 class="w3-center w3-jumbo" style="margin-bottom:64px">About</h1>
    <p>
      <?php echo $about->about_desc; ?>
        
      </p>
    <p><strong>The Chef?</strong> Mr. Italiano himself<img src="<?php echo base_url() ?>/assets/img/chef.jpg" style="width:150px" class="w3-circle w3-right" alt="Chef"></p>
    <p>We are proud of our interiors.</p>
    <img src="<?php echo base_url() ?>/assets/img/onepage_restaurant.jpg" style="width:100%" class="w3-margin-top w3-margin-bottom" alt="Restaurant">
    <h1><b>Opening Hours</b></h1>
    
    <div class="w3-row">
      <div class="w3-col s6">
        <p>Mon & Tue CLOSED</p>
        <p>Wednesday 10.00 - 24.00</p>
        <p>Thursday 10:00 - 24:00</p>
      </div>
      <div class="w3-col s6">
        <p>Friday 10:00 - 12:00</p>
        <p>Saturday 10:00 - 23:00</p>
        <p>Sunday Closed</p>
      </div>
    </div>
    
  </div>
</div>

<!-- Contact (with google maps) -->
<div class="peta">
				<h1 class="judul-map"><strong>JA PANFUD</strong><br> Branch Company</h1>
			<center>	
				<img src="<?php echo base_url() ?>/assets/img/map.png"   alt="" />
			</center>
			<a href="" class="image" ><img src="<?php echo base_url() ?>/assets/img/pin.png" style="margin-top: -9.5%;margin-left: 31%; animation: kdip 2s infinite;"><div class="nama-lokasi" style="margin-left: 31%;margin-top: -11.5%;">Jakarta</div></a>			
			<a href="" class="image" ><img src="<?php echo base_url() ?>/assets/img/pin.png" style="margin-top: -8%;margin-left: 35%; animation: kdip 2s infinite;"><div class="nama-lokasi" style="margin-left: 35%;margin-top: -10%;">Yogyakarta</div></a>
			<a href="" class="image" ><img src="<?php echo base_url() ?>/assets/img/pin.png" style="margin-top: -7.5%;margin-left: 40%; animation: kdip 2s infinite;"><div class="nama-lokasi" style="margin-left: 39%;margin-top: -9.5%;">Surabaya</div></a>
			<a href="" class="image" ><img src="<?php echo base_url() ?>/assets/img/pin.png" style="margin-top: -5%;margin-left: 48%; animation: kdip 5s infinite;"><div class="nama-lokasi" style="margin-left: 47.8%;margin-top: -7%;">NTB</div></a>
			<a href="" class="image" ><img src="<?php echo base_url() ?>/assets/img/pin.png" style="margin-top: -5.3%;margin-left: 54%; animation: kdip 5s infinite;"><div class="nama-lokasi" style="margin-left: 53.5%;margin-top: -7.2%;">NTT</div></a>
			<a href="" class="image" ><img src="<?php echo base_url() ?>/assets/img/pin.png" style="margin-top: -3%;margin-left: 58%; animation: kdip 5s infinite;"><div class="nama-lokasi" style="margin-left: 58.5%;margin-top: -5.5%;">KUPANG</div></a>
			<a href="" class="image" ><img src="<?php echo base_url() ?>/assets/img/pin.png" style="margin-top: -17.5%;margin-left: 71.5%; animation: kdip 1s infinite;"><div class="nama-lokasi" style="margin-left: 70%;margin-top: -19.5%;">Sorong</div></a>
			<a href="" class="image" ><img src="<?php echo base_url() ?>/assets/img/pin.png" style="margin-top: -14.5%;margin-left: 84.5%; animation: kdip 1s infinite;"><div class="nama-lokasi" style="margin-left: 84%;margin-top: -16.5%;">Jaya Pura</div></a>
			<a href="" class="image" ><img src="<?php echo base_url() ?>/assets/img/pin.png" style="margin-top: -5%;margin-left: 85.5%; animation: kdip 1s infinite;"><div class="nama-lokasi" style="margin-left: 87.5%;margin-top: -5.8%;">Merauke</div></a>
			<a href="" class="image" ><img src="<?php echo base_url() ?>/assets/img/pin.png" style="margin-top: -12%;margin-left: 79%; animation: kdip 1s infinite;"><div class="nama-lokasi" style="margin-left: 70%;margin-top: -14%;">Tembaga Pura</div></a>
			<a href="" class="image" ><img src="<?php echo base_url() ?>/assets/img/pin.png" style="margin-top: -21%;margin-left: 56.2%; animation: kdip 2.5s infinite;"><div class="nama-lokasi" style="margin-left: 55%;margin-top: -23%;">Gorontalo</div></a>
			<a href="" class="image" ><img src="<?php echo base_url() ?>/assets/img/pin.png" style="margin-top: -11.8%;margin-left: 51.5%; animation: kdip 2.5s infinite;"><div class="nama-lokasi" style="margin-left: 49%;margin-top: -13.8%;">Pangkajene</div></a>
			<a href="" class="image" ><img src="<?php echo base_url() ?>/assets/img/pin.png" style="margin-top: -18.5%;margin-left: 47.5%; animation: kdip 1.5s infinite;"><div class="nama-lokasi" style="margin-left: 43%;margin-top: -20.5%;">Samarinda</div></a>
			<a href="" class="image" ><img src="<?php echo base_url() ?>/assets/img/pin.png" style="margin-top: -15.5%;margin-left: 40.5%; animation: kdip 1.5s infinite;"><div class="nama-lokasi" style="margin-left: 39%;margin-top: -17.5%;">Sampit</div></a>
			<a href="" class="image" ><img src="<?php echo base_url() ?>/assets/img/pin.png" style="margin-top: -19.5%;margin-left: 35.5%; animation: kdip 1.5s infinite;"><div class="nama-lokasi" style="margin-left: 32%;margin-top: -21.5%;">Pontianak</div></a>
			<a href="" class="image" ><img src="<?php echo base_url() ?>/assets/img/pin.png" style="margin-top: -28%;margin-left: 15%; animation: kdip 6.5s infinite;"><div class="nama-lokasi" style="margin-left: 12%;margin-top: -30.5%;">Aceh</div></a>
			<a href="" class="image" ><img src="<?php echo base_url() ?>/assets/img/pin.png" style="margin-top: -25.5%;margin-left: 18%; animation: kdip 6.5s infinite;"><div class="nama-lokasi" style="margin-left: 17%;margin-top: -27.5%;">Medan</div></a>
			<a href="" class="image" ><img src="<?php echo base_url() ?>/assets/img/pin.png" style="margin-top: -11.5%;margin-left: 27.5%; animation: kdip 6.5s infinite;"><div class="nama-lokasi" style="margin-left: 24%;margin-top: -13.5%;">Lampung</div></a>
			<a href="" class="image" ><img src="<?php echo base_url() ?>/assets/img/pin.png" style="margin-top: -22%;margin-left: 21%; animation: kdip 6.5s infinite;"><div class="nama-lokasi" style="margin-left: 20.6%;margin-top: -24%;">Pakanbaru</div></a>
			
</div>


<!-- Footer -->
<footer class="w3-center w3-black w3-padding-48 w3-xxlarge">
  <p>Powered by <a href="https://www.w3schools.com/w3css/default.asp" title="W3.CSS" target="_blank" class="w3-hover-text-green">w3.css</a></p>
</footer>

<!-- Add Google Maps -->
<script>

// Tabbed Menu
function openMenu(evt, menuName) {
  var i, x, tablinks;
  x = document.getElementsByClassName("menu");
  for (i = 0; i < x.length; i++) {
     x[i].style.display = "none";
  }
  tablinks = document.getElementsByClassName("tablink");
  for (i = 0; i < x.length; i++) {
     tablinks[i].className = tablinks[i].className.replace(" w3-red", "");
  }
  document.getElementById(menuName).style.display = "block";
  evt.currentTarget.firstElementChild.className += " w3-red";
}
document.getElementById("myLink").click();
</script>
<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyBu-916DdpKAjTmJNIgngS6HL_kDIKU0aU&callback=myMap"></script>
<!--
To use this code on your website, get a free API key from Google.
Read more at: https://www.w3schools.com/graphics/google_maps_basic.asp
-->



</body>
</html>
