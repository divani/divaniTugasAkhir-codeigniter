<?php
defined('BASEPATH') OR exit ('No direct script access allowed');

class Admin extends CI_Controller {

public function __construct()
	{
		parent::__construct();
		$this->load->model('admin_model');
	}


	public function data_view()
	{ 
        		$data['user'] = $this->admin_model->get_user();		
        		$data['data_makanan'] = $this->admin_model->get_makanan();		
			redirect('admin/data_view',$data);
		
	}


	    public function delete_user($id_user)
	    {
	    	$this->admin_model->delete_user($id_user);
	    	redirect('user/data_halaman');
	    }

	    public function delete_makanan($id_makanan)
	    {
	    	$this->admin_model->delete_makanan($id_makanan);
	    	redirect('user/data_halaman');
	    }
	   
}

    
	


?>