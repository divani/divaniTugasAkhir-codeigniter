<?php
defined('BASEPATH') OR exit ('No direct script access allowed');

class Admin_model extends CI_Model{
	
	public function get_sushi()
	{
		$cat = "Sushi";
		return  $this->db->where('kategori_makanan', $cat)
					->get('makanan')
					->result();
	}	

	public function get_ramen()
	{
		$cat = "Ramen";
		return  $this->db->where('kategori_makanan', $cat)
					->get('makanan')
					->result();
	}	
	public function get_salads()
	{
		$cat = "Salads";
		return  $this->db->where('kategori_makanan', $cat)
					->get('makanan')
					->result();
	}
	public function get_about()
	{
		return $this->db->get('about')
						->row();
	}
	public function get_contact()
	{
		return $this->db->get('contact')
						->row();
	}
}
?>