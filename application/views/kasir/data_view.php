<!DOCTYPE html>
<html>
<title>Ja Panfud</title>
<meta charset="UTF-8">
<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="icon" type="image/png" href="<?php echo base_url() ?>/assets/img/logo.png">
<link rel="stylesheet" href="<?php echo base_url() ?>/assets/css/w3.css">
<style>
body, html {height: 100%;  background:url('<?php echo base_url() ?>/assets/img/back2.jpg');}
body,h1,h2,h3,h4,h5,h6 {font-family: "Amatic SC", sans-serif}
.menu {display: none}
.bgimg {
    background-repeat: no-repeat;
    background-size: cover;
    background-image: url("<?php echo base_url() ?>/assets/img/restaurant.jpg");
    min-height: 100%;
}
.btn:hover{
    opacity: 0.8;
}

.a{ 
  position:absolute;
  background:rgba(30,30,30,0.7);
  display:none;
  margin-top:40px;
  margin-left:90%;
  border-radius:0 0 30px 30px;
}
.b:hover .a{
  display:block;
}
ul{
  list-style: none;
  width: 130px;
}
ul li{
  margin-left: -45%;
  padding:0.5%;
  padding-left:30%;
  padding-right:30%;
}

ul li:hover{
  background:rgba(100,100,100,0.7);
}

.kotak{
  padding: 5%;
  width: 30%;
  margin-left: 3%;
  float: left;
  background: rgba(30,30,30,0.8);
  margin-top: 2% ;
  border: 2px solid black;
  border-radius: 10px;
}
.kotak:hover{
    border: 2px solid red;
    background: none;
    animation: 1s wow;
}
@keyframes wow{
  0%{
     background: rgba(30,30,30,0.8);
     border: 2px solid black;
  }
  100%{
     border: 2px solid red;
    background: none;
  }
}
</style>
<body>
<header class="bgimg w3-display-container w3-grayscale-min" id="home">
  <div class="w3-display-bottomleft w3-padding">
    <span class="w3-tag w3-xlarge">Welcome <?php echo $username ?> !</span>
  </div>
  <div class="w3-display-middle w3-center">
    <img src="<?php echo base_url(); ?>assets/img/work.png" width="20%"><br>
    <span class="w3-text-white  w3-hide-small" style="font-size:100px;background: rgba(0,0,0,0.4);border-radius: 30px">ENJOY YOUR WORK</span>

    <p><a href="chart" class="w3-button w3-xxlarge w3-black">Add the new TRANSACTION !</a></p>
  </div>
</header>
<!-- Navbar (sit on top) -->
<div class="w3-top w3-hide-small">
  <div class="w3-bar w3-xlarge w3-black w3-opacity w3-hover-opacity-off" id="myNavbar">
    <a href="#home" class="w3-bar-item w3-button">HOME</a>
    <a href="chart" class="w3-bar-item w3-button">TRANSACTION</a>
    <div class="b"><a  style="text-decoration:none;float: right;margin-right: 2%;margin-top: 0.8%;font-size: 20px"><?php echo $username ?> | <?php echo $level ?>
    <div class="a">
        <ul>
         
          <li>
          <a href="logout" style="text-decoration:none;width:5%;">Logout</a>
          </li>
      </ul>
      </div></a></div>
  </div>
</div>
  
<!-- Header with image -->





<!-- Add Google Maps -->
<script>
// Tabbed Menu
function openMenu(evt, menuName) {
  var i, x, tablinks;
  x = document.getElementsByClassName("menu");
  for (i = 0; i < x.length; i++) {
     x[i].style.display = "none";
  }
  tablinks = document.getElementsByClassName("tablink");
  for (i = 0; i < x.length; i++) {
     tablinks[i].className = tablinks[i].className.replace(" w3-red", "");
  }
  document.getElementById(menuName).style.display = "block";
  evt.currentTarget.firstElementChild.className += " w3-red";
}
document.getElementById("myLink").click();
</script>
<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyBu-916DdpKAjTmJNIgngS6HL_kDIKU0aU&callback=myMap"></script>
<!--
To use this code on your website, get a free API key from Google.
Read more at: https://www.w3schools.com/graphics/google_maps_basic.asp
-->
 
<script src="<?php echo base_url(); ?>bower_components/jquery/dist/jquery.min.js"></script>
<script src="<?php echo base_url(); ?>bower_components/jquery-ui/jquery-ui.min.js"></script>
<script src="<?php echo base_url(); ?>bower_components/bootstrap/dist/js/bootstrap.min.js"></script>


    <script type="text/javascript">
        $('.update_user').on('click',function()
        {
            var id =$(this).attr('data-id');
            $('#id_user').val(id);

             var name =$(this).attr('data-name');
            $('#nama_user').val(name);

             var username =$(this).attr('username');
            $('#username').val(username);

             var password =$(this).attr('password');
            $('#password').val(password);

             var level =$(this).attr('level');
            $('#level').val(level);

        })
    </script>


</body>
</html>

