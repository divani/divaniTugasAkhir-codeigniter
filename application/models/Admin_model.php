<?php
defined('BASEPATH') OR exit ('No direct script access allowed');

class Admin_model extends CI_Model{
	
	public function get_sushi()
	{
		$cat = "Sushi";
		return  $this->db->where('kategori_makanan', $cat)
					->get('makanan')
					->result();
	}	

	public function get_ramen()
	{
		$cat = "Ramen";
		return  $this->db->where('kategori_makanan', $cat)
					->get('makanan')
					->result();
	}	
	public function get_salads()
	{
		$cat = "Salads";
		return  $this->db->where('kategori_makanan', $cat)
					->get('makanan')
					->result();
	}
	public function get_user()
	{
		return  $this->db->get('user')
					->result();
	}

	public function get_makanan()
	{
		return  $this->db->get('makanan')
					->result();
	}
	public function get_about()
	{
		return $this->db->get('about')
						->row();
	}
	public function get_contact()
	{
		return $this->db->get('contact')
						->row();
	}
	public function delete_makanan($id_makanan){

		$this->db->where('id_makanan',$id_makanan);
		$this->db->delete('makanan');
		return TRUE;
		
	}

	public function delete_user($id_user)
	{
		$this->db->where('id_user',$id_user);
		$this->db->delete('user');
		return TRUE;
		
	}

	
	public function update_user($id_user, $nama_user , $username, $password)
	{
		$this->db->set('id_user', $id_user);
		$this->db->set('nama_user', $nama_user);
		$this->db->set('username', $username);
		$this->db->set('password', $password);
		$this->db->where('id_user', $id_user);
		 if($this->db->update('user'))
		 {
		 	return 1;
		 } else {
		 	return 0;
		 }
	}	
}
?>