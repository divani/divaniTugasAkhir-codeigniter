<!DOCTYPE html>
<html>
<title>Ja Panfud</title>
<meta charset="UTF-8">
<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="icon" type="image/png" href="<?php echo base_url() ?>/assets/img/logo.png">
<link rel="stylesheet" href="<?php echo base_url() ?>/assets/css/w3.css">
<style>
body, html {height: 100%}
body,h1,h2,h3,h4,h5,h6 {font-family: "Amatic SC", sans-serif}
.menu {display: none}
.bgimg {
    background-repeat: no-repeat;
    background-size: cover;
    background-image: url("<?php echo base_url() ?>/assets/img/sushii.jpg");
    min-height: 100%;
}
.btn:hover{
    opacity: 0.8;
}
td{
  width: 250px;
}
td input{
  padding-left: 2%;
   padding-right: 2%;
  width: 300px;
  border-radius: 10px;
}
td textarea{
  padding-left: 2%;
   padding-right: 2%;
   width: 300px;
  border-radius: 10px;
}

.a{ 
  position:absolute;
  background:rgba(30,30,30,0.7);
  display:none;
  margin-top:40px;
  margin-left:90%;
  border-radius:0 0 30px 30px;
}
.b:hover .a{
  display:block;
}
ul{
  list-style: none;
  width: 130px;
}
ul li{
  margin-left: -45%;
  padding:0.5%;
  padding-left:30%;
  padding-right:30%;
}

ul li:hover{
  background:rgba(100,100,100,0.7);
}
</style>    
<body>

<!-- Navbar (sit on top) -->
<div class="w3-top w3-hide-small">
  <div class="w3-bar w3-xlarge w3-black w3-opacity w3-hover-opacity-off" id="myNavbar">
    <a href="<?php base_url()?>../data_halaman#menu" class="w3-bar-item w3-button">FOOD</a>
    <a href="<?php base_url()?>../data_halaman#user" class="w3-bar-item w3-button">USER</a>
   <div class="b"><a  style="text-decoration:none;float: right;margin-right: 2%;margin-top: 0.8%;font-size: 20px"><?php echo $username ?> | <?php echo $level ?>
    <div class="a">
        <ul>
          
          <li>
          <a href="logout" style="text-decoration:none;width:5%;">Logout</a>
          </li>
      </ul>
      </div></a></div>
  </div>
</div>
  
<!-- Header with image -->


<!-- Menu Container -->
<div class="w3-container w3-green w3-padding-64 w3-xxlarge" id="menu">
  <div class="w3-content">
 
<?php if (!empty ($notif)) {
    echo '<div class="alert alert-danger">'.$notif.'</div>
    ';
}?>

    <form action="<?php echo base_url('');?>index.php/user/edit_makann/<?php echo $meds->id_makanan ?>" method="post" enctype="multipart/form-data">

                           <center> <h1><strong>Edit</strong> </h1>
                               <div style="font-size: 20px;font-family: tw cen mt;padding: 2.5% ">
                                <table>
                                  <div class="form-group">
                                    <input class="form-control" type="hidden" name="id_makanan" value="<?php echo $meds->id_makanan ?>">
                                </div>
                                  <tr>
                                <div class="form-group">
                                   <td> <label>Nama Makanan</label></td>
                                    <td><input class="form-control" type="text" placeholder="Enter text" name="nama_makanan" value="<?php echo $meds->nama_makanan ?>"></td>
                                </div></tr><tr>
                                <div class="form-group">
                                    <td><label>Harga Makanan</label></td>
                                    <td><input class="form-control" placeholder="Enter text" name="harga_makanan" value="<?php echo $meds->harga_makanan ?>"></td>
                                </div></tr><tr>
                                <div class="form-group">
                                    <td><label>Deskripsi Makanan</label></td>
                                    <td><textarea class="form-control" placeholder="Enter text" type="text-box" name="deskripsi_makanan" value="<?php echo $meds->deskripsi_makanan ?>"><?php echo $meds->deskripsi_makanan ?></textarea> </td>
                                </div></tr>
                                 
                                <tr>
                                <div class="form-group">
                                      <td>
                                       <label>Pilih Kategori</label>
                                    </td><td><select class="form-control" name="kategori_makanan" >
                                    
                                            <option value="Sushi">Sushi</option>
                                            <option value="Salads">Salad</option>
                                            <option value="Ramen">Ramen</option>
                                    </select></td>
                                </div></tr>
                                </table>
                              </div>

                                <input type="submit" class="btn btn-primary"  style="border-radius: 30px;padding-left: 4%;padding-right: 4%;font-size: 26px" value="Submit" name="submit">
                              </center>
        </form>

  </div>
</div>



<!-- Footer -->
<footer class="w3-center w3-black w3-padding-48 w3-xlarge">
  <p>Powered by <a href="https://www.w3schools.com/w3css/default.asp" title="W3.CSS" target="_blank" class="w3-hover-text-green">w3.css</a></p>
</footer>

<!-- Add Google Maps -->
<script>
// Tabbed Menu
function openMenu(evt, menuName) {
  var i, x, tablinks;
  x = document.getElementsByClassName("menu");
  for (i = 0; i < x.length; i++) {
     x[i].style.display = "none";
  }
  tablinks = document.getElementsByClassName("tablink");
  for (i = 0; i < x.length; i++) {
     tablinks[i].className = tablinks[i].className.replace(" w3-red", "");
  }
  document.getElementById(menuName).style.display = "block";
  evt.currentTarget.firstElementChild.className += " w3-red";
}
document.getElementById("myLink").click();
</script>
<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyBu-916DdpKAjTmJNIgngS6HL_kDIKU0aU&callback=myMap"></script>
<!--
To use this code on your website, get a free API key from Google.
Read more at: https://www.w3schools.com/graphics/google_maps_basic.asp
-->
 
<script src="<?php echo base_url(); ?>bower_components/jquery/dist/jquery.min.js"></script>
<script src="<?php echo base_url(); ?>bower_components/jquery-ui/jquery-ui.min.js"></script>
<script src="<?php echo base_url(); ?>bower_components/bootstrap/dist/js/bootstrap.min.js"></script>


    <script type="text/javascript">
        $('.update_user').on('click',function()
        {
            var id =$(this).attr('data-id');
            $('#id_user').val(id);

             var name =$(this).attr('data-name');
            $('#nama_user').val(name);

             var username =$(this).attr('username');
            $('#username').val(username);

             var password =$(this).attr('password');
            $('#password').val(password);

             var level =$(this).attr('level');
            $('#level').val(level);

        })
    </script>


</body>
</html>

