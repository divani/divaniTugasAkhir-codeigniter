<!DOCTYPE html>
<html>
<title>Tugas Akhir</title>
<meta charset="UTF-8">

		<link rel="icon" type="image/png" href="<?php echo base_url() ?>/assets/img/logo.png">
<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="stylesheet" href="<?php echo base_url() ?>/assets/css/w3.css">
<link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Amatic+SC">
<style>
body, html {height: 100%}
body,h1,h2,h3,h4,h5,h6 {font-family: "Amatic SC", sans-serif}
.menu {display: none}
.bgimg {
    background-repeat: no-repeat;
    background-size: cover;
    background-image: url("<?php echo base_url() ?>/assets/img/sushii.jpg");
    min-height: 100%;
}
.btn:hover{
    opacity: 0.8;
}
.form-control{
  margin-bottom: 0.5%;
  border-radius: 30px;
  padding-left: 3%;
  padding-right: 3%; 
}
.btnn{
    margin-bottom:1%;
    margin-top:5%;
    background: white;
    color: navy;
    text-decoration: none;
    font-family: tw cen mt;
    padding: 1%;
    padding-left: 10%;
    padding-right: 10%;
    border-radius: 30px;
    font-size: 20px;
    box-shadow: 0 0 1px #fff;
    border: 2px solid white;
}

.btnn:hover{
    color: white;
    background: none;
    animation: wss 2s;
    border: 2px solid white;
}
@keyframes wss{
    0%{
        background:rgba(225,225,225,0.8);
    }
    100%{
        background:none;
    }
}
.link{
    color: white;
    text-decoration: none;
}
.link:hover{
    color: gray;
}
</style>
<body>

<!-- Header with image -->
<header class="bgimg w3-display-container w3-grayscale-min" id="home">
  <div class="w3-display-bottomleft w3-padding">
    <span class="w3-tag w3-xlarge">Open from 10am to 12pm</span>
  </div>
  <div class="w3-display-middle w3-center">
    <span class="w3-text-white  w3-hide-small" style="font-size:80px">JA PANFUD<p  style="font-size:40px;margin-top: -3%;margin-bottom: 3%;">Log In</p></span>
   
    <div class="container" >
        <div class="row" >
            <div class="col-md-4 col-md-offset-4" >
                <div class="login-panel panel panel-default" >
                    <div class="panel-body" >
                        <?php
                            $notifikasi = $this->session->flashdata('notif');
                            if($notifikasi != NULL)
                            {
                                echo '
                                    <div class="alert alert-info" style="color:white;font-family:tw cen mt;">
                                    '.$notifikasi.'
                                    </div>
                                ';
                            }
                        ?>
                        <form role="form" action="<?php echo base_url('index.php/user/do_login'); ?>" method="post" >
                            <fieldset style="border-radius: 3%;padding-left: 7%;padding-right: 6%;">
                                <!-- <div class="form-group">
                                    <input class="form-control" placeholder="E-mail" name="email" type="email" autofocus>
                                </div> -->
                                <div class="form-group"  style="margin-top:10%;font-family: tw cen mt">
                                    <input class="form-control" placeholder="Username" name="username" type="text" autofocus>
                                </div>
                                <div class="form-group" style="font-family: tw cen mt">
                                    <input class="form-control" placeholder="Password" name="password" type="password" value="">
                                </div>
                                <!-- Change this to a button or input when using this as a form -->
                                <a><input type="submit" name="submit" value="Let's log in !" class="btnn" ></a><br>
                                <a class="link" style="font-family: tw cen mt;" href="signup">Don't have an account ? Sign Up in here</a>
                            </fieldset>
                        </form>
                    </div>
                </div>
            </div>
        </div>
  </div>
</header>

  


    <!-- jQuery -->
    <script src="<?php echo base_url() ?> /assets_admin/vendor/jquery/jquery.min.js"></script>

    <!-- Bootstrap Core JavaScript -->
    <script src="<?php echo base_url() ?> /assets_admin/vendor/bootstrap/js/bootstrap.min.js"></script>

    <!-- Metis Menu Plugin JavaScript -->
    <script src="<?php echo base_url() ?> /assets_admin/vendor/metisMenu/metisMenu.min.js"></script>

    <!-- Custom Theme JavaScript -->
    <script src="<?php echo base_url() ?> /assets_admin/dist/js/sb-admin-2.js"></script>
</div>



</body>
</html>
