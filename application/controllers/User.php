<?php
defined('BASEPATH') OR exit ('No direct script access allowed');

class User extends CI_Controller {
public function __construct()
	{
		parent::__construct();
		$this->load->model('user_model');
	}

	public function index()
	{
		redirect('user/login');
	}
	public function chart()
	{
		$id_user = $this->session->userdata('id_user');
		 $data['makanan'] = $this->user_model->get_makanan();
	    $data['data_chart']=$this->user_model->data_chart($id_user);	  
		$data['username']=$this->session->userdata('username');	  
		$data['level']=$this->session->userdata('level');
		$this->load->view('user/chart',$data);
	}

	public function login()
	{
		 
		if($this->session->userdata('logged_in') == FALSE)
		{
			$this->load->view('user/login_view');

		} else {
			redirect('user/data_halaman');
		}
	}

       

	public function do_login()
	{
		if($this->input->post('submit'))
		
			$this->form_validation->set_rules('username' , 'Username ','trim|required');
			$this->form_validation->set_rules('password' , 'Password ','trim|required');

		if($this->form_validation->run() == TRUE)

			{
				if($this->user_model->check_user() == TRUE)
					{
						$data['notif'] = 1;
						echo json_encode($data);
					
							redirect('user/data_halaman');
				} else {
					$this->session->set_flashdata('notif','Username atau password salah !');
					redirect('user/login');
				}
		} else{
			redirect('user');
		}
	}

	public function logout()
	{
			$data = array(
				'id_user'	=> "",
				'username'	=>"",
				'logged_in'	=> FALSE
			);

			$this->session->unset_userdata($data);
			$this->session->sess_destroy();

			redirect('user/login');

	}

		public function data_halaman()
		{
			if($this->session->userdata('logged_in') == TRUE)
			{		
						 $data['id_user'] = $this->session->userdata('id_user');
						 $data['username'] = $this->session->userdata('username');
						 $data['level'] = $this->session->userdata('level');
						 $data['sushi'] = $this->user_model->get_sushi();
        				 $data['ramen'] = $this->user_model->get_ramen();
        				 $data['salads'] = $this->user_model->get_salads();
        				 $data['about'] = $this->user_model->get_about();
        				 $data['user'] = $this->user_model->get_user();		
        				 $data['trans'] = $this->user_model->get_transaksi();		
        				 $data['contact'] = $this->user_model->get_contact();
        		 if ($this->session->userdata('level') == "admin"){
							
						$this->load->view('admin/data_view',$data);
					}

        		 else if ($this->session->userdata('level') == "kasir"){
							
						$this->load->view('kasir/data_view',$data);
					}
					else{

						 
						$this->load->view('user/home_view',$data);
					}
					
				
					
			} else{
				redirect('user/login');
			}
			
		}

	public function signup()
	{
		 
		if($this->session->userdata('logged_in') == FALSE)
		{
			$this->load->view('user/signup_view');

		} else {
			redirect('user/login');
		}
	}

		public function do_daftar()
	    {
	        $this->form_validation->set_rules('nama_user', 'nama_user', 'trim|required');
	        $this->form_validation->set_rules('username', 'username', 'trim|required');
	        $this->form_validation->set_rules('password','password','trim|required');


	        if ($this->form_validation->run() === FALSE) {
	            $data['main_view'] = 'user/tambah_user';
	            $this->load->view('user/signup', $data);
	        }else{
	            $this->user_model->tambah_user();
	            redirect('user/login');
	        }
	    }
		public function tambah_saran()
	    {
	        $this->form_validation->set_rules('tentang', 'tentang_saran', 'trim|required');
	        $this->form_validation->set_rules('saran', 'desc_saran', 'trim|required');

	        if ($this->form_validation->run() === FALSE) {
	            $data['main_view'] = 'user/tambah_saran';
	            $this->load->view('user/home_view', $data);
	        }else{
	            $this->user_model->tambah_saran();
	            redirect('user/data_halaman');
	        }
	    }
	    public function addchart($id_makanan, $nama_makanan, $harga_makanan)
	    {		
	    		$nama = urldecode($nama_makanan);
	            $this->user_model->addchart($id_makanan,$nama,$harga_makanan);
	            redirect('user/chart');
	        
	    }
	    public function deletechart($id_keranjang)
	    {		

	            $this->user_model->delete_chart($id_keranjang);
	            redirect('user/chart');
	        
	    }
	    public function addtransaction($id_keranjang){

	    		$this->user_model->addtransaction($id_keranjang);
	            redirect('user/chart');
	    }
	    public function edit_makanan()
		 	{			
		 				$id = $this->uri->segment(3);
		 				$data['username']=$this->session->userdata('username');	  
		$data['level']=$this->session->userdata('level');
		 				$data['meds'] = $this->user_model->getDetailMakanan($id);
		 				$this->load->view('admin/edit_makanan_view',$data);
		 	}
		 	public function edit_makann()
		 	{
		 		$id = $this->uri->segment(3);
		 			if ($this->input->post('submit')) {
		 			
		 				if ($this->user_model->update_makanan($id)) {
		 						$data['notif'] = "Edit Success!";
		 					redirect(base_url('index.php/user/data_halaman#menu'));
		 				

		 			} else {
		 					$data['notif'] = validation_errors();
		 					$this->load->view('admin/edit_makanan_view',$data);
		 				}	 				
		 		} 	
		 	}
		    public function edit_user()
		 	{			
		 				$id = $this->uri->segment(3);
		 				$data['username']=$this->session->userdata('username');	  
						$data['level']=$this->session->userdata('level');
		 				$data['meds'] = $this->user_model->getDetailUser($id);
		 				$this->load->view('admin/edit_user_view',$data);
		 	}
		 	public function edit_userr()
		 	{
		 		$id = $this->uri->segment(3);
		 			if ($this->input->post('submit')) {
		 			
		 				if ($this->user_model->update_user($id)) {
		 						$data['notif'] = "Edit Success!";
		 					redirect(base_url('index.php/user/data_halaman#user'));
		 				

		 			} else {
		 					$data['notif'] = validation_errors();
		 					$this->load->view('admin/edit_makanan_view',$data);
		 				}	 				
		 		} 	
		 	}

	    public function delete_trans($id)
	    {
	    	$this->user_model->delete_trans($id);
	    	redirect('user/data_halaman#transaction');
	    }

		 	public function makanann()
		 	{
		 		if ($this->input->post('submit')) 
		 				$config['upload_path'] = './upload/';
		 				$config['allowed_types'] = 'gif|jpg|png';
		 				$config['max_size']  = '25000';
		 				
		 				$this->load->library('upload');
		 				$this->upload->initialize($config);
		 				
		 				if ($this->upload->do_upload('foto')){
		 					if ($this->user_model->save_makanan($this->upload->data()) == True) {
		 						$data['notif'] = 'Success Added';
		 						redirect('user/data_halaman',$data);
		 					} else {
		 						$data['notif'] = 'Failled Added';
		 						redirect('user/tambah_makanan',$data);
		 					}
		 				}else{
		 					$data['notif'] = $this->upload->display_errors();
		 						redirect('user/tambah_makanan',$data);

		 				}
		 				
		 			} 
		 			public function userr()
		 			{
		 				
		 					if ($this->user_model->save_user() ){
		 						$data['notif'] = 'Success Added';
		 						redirect('user/data_halaman',$data);
		 					} else {
		 						$data['notif'] = 'Failled Added';
		 						redirect('user/tambah_makanan',$data);
		 				}
		 				
		 			} 
		 		
		 	 public function tambah_makanan()
		 	{
					 $data['username']=$this->session->userdata('username');	  
						$data['level']=$this->session->userdata('level');
					 $this->load->view('admin/tambah_makanan_view',$data);
		 	}
 				public function tambah_user()
		 	{
					 $data['username']=$this->session->userdata('username');	  
						$data['level']=$this->session->userdata('level');
					 $this->load->view('admin/tambah_user_view',$data);
		 	}

		public function  addcart($id)
			{
					$detail=$this->user_model->detail($id);
					$data = array (
						'id'	=> $detail->id_makanan,
						'qty'	=>1,
						'price'	=>$detail->harga_makanan,
						'name'	=>$detail->nama_makanan,
					);
					$this->cart->insert($data);
					redirect('user/chart','refresh');
			}
	public function hapuscart($id)
	{
		$data = array (
					'rowid'=> $id,
					'qty'  =>0
		);
		$this->cart->update($data);
		redirect('user/chart','refresh');
	}
	public function ubahqty($id)
	{
		$data = array(
					'rowid'	=> $id,
					'qty'	=>$this->input->post('qty')
		);
		$this->cart->update($data);
		redirect('user/chart','refresh');
	}	
	public function checkout()
	{
		$kembalian = $this->cart->total() - $this->input->post('uang');
		if ($this->input->post('uang')<$this->cart->total()) {
			$this->session->set_flashdata('pesan', 'Uang Kurang');
		} else {
			if ($this->user_model->simpanTrans()) {
				     $lastTrans=$this->user_model->lastTrans()->id_transaksi;
				     $this->session->set_flashdata('pesan', 'kembaliannya : '.$kembalian);
				 $this->session->set_flashdata('pesan_print', 
					'<a href="cetak/'.$lastTrans.'">Cetak Nota</a>');
				$this->cart->destroy();			
			}
			else{
				$this->session->set_flashdata('pesan', 'error');
			}
		}
		redirect('user/chart','refresh');
	}	 
	public function cetak($id)
	{
		$data['tampil_nota']=$this->user_model->getDataNota($id)->row();
		$data['detail_nota']=$this->user_model->getDataNota($id)->result();
		$data['tampil_transaksi']=$this->user_model->getDataTransaksi($id);
		$this->load->view('nota', $data);
	}
	}
		
?>