<!DOCTYPE html>
<html>
<title>Ja Panfud</title>
<meta charset="UTF-8">
<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="icon" type="image/png" href="<?php echo base_url() ?>/assets/img/logo.png">
<link rel="stylesheet" href="<?php echo base_url() ?>/assets/css/w3.css">
<link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Amatic+SC">
<style>
body, html {height: 100%}
body,h1,h2,h3,h4,h5,h6 {font-family: "Amatic SC", sans-serif}
.menu {display: none}
body {
    background-size: cover;
    background-image: url("<?php echo base_url() ?>/assets/img/sushii.jpg");
    min-height: 100%;
}
.btn:hover{
    opacity: 0.8;
}
#home{
  color: white;
}
.a{ 
  position:absolute;
  background:rgba(30,30,30,0.7);
  display:none;
  margin-top:40px;
  margin-left:90%;
  border-radius:0 0 30px 30px;
}
.b:hover .a{
  display:block;
}
ul{
  list-style: none;
  width: 130px;
}
ul li{
  margin-left: -45%;
  padding:0.5%;
  padding-left:30%;
  padding-right:30%;
}

ul li:hover{
  background:rgba(100,100,100,0.7);
}
</style>
<body >


<!-- Navbar (sit on top) -->
<div class="w3-top w3-hide-small">
  <div class="w3-bar w3-xlarge w3-black w3-opacity w3-hover-opacity-off" id="myNavbar">
    <a href="data_halaman" class="w3-bar-item w3-button">HOME</a>
    <a href="chart" class="w3-bar-item w3-button">TRANSACTION</a>
    <div class="b"><a  style="text-decoration:none;float: right;margin-right: 2%;margin-top: 0.8%;font-size: 20px"><?php echo $username ?> | <?php echo $level ?>
    <div class="a">
        <ul>
         
          <li>
          <a href="logout" style="text-decoration:none;width:5%;">Logout</a>
          </li>
      </ul>
      </div></a></div>
  </div>
</div>
  
<!-- Header with image -->
<header class="bgimg w3-display-container w3-grayscale-min" id="home" style="padding-top: 3%;font-size: 35px"></header>
  <div class="container">

    <div class="row">
      <div style="margin: 2%;width: 50%;margin-left: 5%;background: rgba(0,0,0,0.8);padding: 2%;border-radius: 30px;float: left;color: white">
        <center><h1 style="font-size: 50px;">Cart</h1></center>
  <?php
        foreach($makanan as $s){ ?>
              
            <h1><b>  </b> <span class="w3-right w3-tag w3-dark-grey w3-round"><?php echo $s->harga_makanan  ?> K</span></h1>
             <?php echo $s->nama_makanan  ?>
            
           <form class="col-md-4" method="POST" action="<?php echo base_url();?>index.php/user/addcart/<?php echo $s->id_makanan ?>">
          <input  style="font-size: 25px" type="submit" class="delete btn btn-danger"  style="margin: 10px;" aria-hidden="true" value="Buy"> </form>
             <hr>
             
   <?php }; ?>
   </div>
</div>
 <div style="margin: 2%;width: 40%;margin-left: 5%;background: rgba(0,0,0,0.8);padding: 2%;border-radius: 30px;float: right;color: white;margin-left: 0%;font-family: tw cen mt">
   <div class="panel-heading">
      <center><h2 class="panel-title"><strong>Shop </strong></h2></center>
    </div>
    <?php if ($this->cart->contents()!=NULL): ?>
      <table  id="example" class="table">
        <thead>
          <th>Id buku</th>
          <th>Judul buku</th>       
          <th>Harga</th>
          <th>Qty</th>
          <th>Aksi</th>
        </thead>
        <tbody>       
          <?php foreach ($this->cart->contents() as $items): ?>
            <tr>
              <td><?=$items['id']?></td>
              <td><?=$items['name']?></td>
              <td>Rp.<?=number_format($items['price'])?></td> 
              <td>
                <form method="POST" action="<?=base_url('index.php/user/ubahqty/'.$items['rowid'])?>">
                  <input onchange="submit()" name="qty" class="form-control" type="text" value="<?=$items['qty']?>">
                </form>
              </td>         
              <td><center><a style="text-decoration: none" class="btn btn-danger" href="<?=base_url('index.php/user/hapuscart/'.$items['rowid'])?>">x</a></center></td>
            </tr>

          <?php endforeach ?>
          <form method="POST" action="<?=base_url('index.php/user/checkout')?>">
            <?php foreach ($this->cart->contents() as $items): ?>
              <input type="hidden" name="qty[]" value="<?=$items['qty']?>">
              <input type="hidden" name="id_makanan[]" value="<?=$items['id']?>"> 
            <?php endforeach ?>
            <tr style="background-color:gray; color: white">
              <td colspan="2">GrandTotal</td><td>Rp. <?=number_format($this->cart->total())?></td><td colspan="2"></td>         
            </tr>
            <tr>
              <td>Uang Bayar</td>

              <td>
                <input required type="number" name="uang" class="form-control">
              </td>

              <td>Pembeli </td>
              <td>
                <input required type="text" name="nama_pembeli" class="form-control">
              </td>
              <td>
                <input type="submit" class="btn btn-success" value="Pay" name="bayar">
              </form>
            </td>
          </tr>
        </tbody>
      </table>    
    <?php else: ?>
      <center>Cart Kosong</center>
    <?php endif ?>  
     <?php if ($this->session->flashdata('pesan')!=null): ?>
      <div class="alert alert-info alert-dismissible" role="alert">
        <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <?=$this->session->flashdata('pesan')?>
      </div>
    <?php endif ?>  
    <?php if ($this->session->flashdata('pesan_print')!=null): ?>
      <div class="alert alert-info alert-dismissible" role="alert">
        <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <?=$this->session->flashdata('pesan_print')?>
      </div>
    <?php endif ?>
   </div>
 </div>

   <div style="margin: 2%;width: 36%;margin-left: 5%;padding: 2%;border-radius: 30px;float: right;color: black;margin-bottom: -2%;">
        <img src="<?=base_url()?>/assets/img/cheff.jpg" width="100%" >
   </div>
   <div style="margin: 2%;width: 36%;margin-top:0;margin-left: 5%;background: rgba(230,230,230,0.8);padding: 2%;border-radius: 30px;float: right;color: black;font-size: 15px;font-family: tw cen mt">
        'Arigato, semoga Anda senang dengan pelayanan kami'   - Ja Panfud
   </div>
   <div style="margin: 2%;width: 36%;margin-left: 5%;background: rgba(225,175,100,0.95);padding: 2%;border-radius: 30px;float: right;color: black;font-size: 17px;font-family: tw cen mt;margin-top: 0;">
    <div style="width: 100%;background: rgba(225,225,225,0.95);padding: 2%;border-radius: 30px;float: right;color: black;font-size: 25px;font-family: Amatic SC;margin-bottom: 5%">
      <CENTER><strong>JA PANFUD</strong></CENTER>
    </div>
        Kami menyediakan banyak pilihan menu makanan Jepang. Untuk Anda pecinta makanan Jepang jangan sampai melewatkan restoran kami dengan tanpa memesan atau mencoba apa pun, sekian Terima Kasih
   </div>
   </div>
   </div>
</header>

<!-- Footer -->



</body>
</html>
