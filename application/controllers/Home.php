<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Home extends CI_Controller {
    
    public function __construct()
    {
        parent::__construct();
        //load home model
        $this->load->model('Home_model');
    }

	public function index()
	{
        $data['sushi'] = $this->Home_model->get_sushi();
        $data['ramen'] = $this->Home_model->get_ramen();
        $data['salads'] = $this->Home_model->get_salads();
        $data['about'] = $this->Home_model->get_about();
        $data['contact'] = $this->Home_model->get_contact();
        $this->load->view('home_view',$data); 
	}
}
?>

        